<?php
// Text
$_['text_items']    = 'Товаров %s (%s)';
$_['text_empty']    = 'Ваша корзина пуста!';
$_['text_cart']     = 'Перейти в корзину';
$_['text_checkout'] = 'Оформить заказ';
$_['text_prod'] = 'Продолжить покупки';
$_['text_recurring']  = 'Платежный профиль';
$_['text_shipment']  = 'Доставка';
$_['text_shipment_clarification']  = 'Уточняйте у оператора';
